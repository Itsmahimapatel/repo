package com.stockmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootStockmarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStockmarketApplication.class, args);
	}

}
