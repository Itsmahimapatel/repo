package com.stockmarket.service;

import com.stockmarket.dao.UserRepository;
import com.stockmarket.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;
    private UserTransport userTransport;
    public int create(String email, String pass, String fname, String lname){
        User use=userRepository.findUserByUseremail(email);
        if(use.getUseremail()==null) {
            User nuser = new User(email, pass, fname, lname);
            userRepository.save(nuser);
            return 1;
        }
        else
            return 0;

    }
    public int login(String email, String pass){
        User nu=userRepository.findUserByUseremail(email);
        if(nu!=null){
            userTransport.setuser(nu);
            return 1;
        }
        else
            return 0;
    }
    public String display(){
        return userTransport.getuser().getFirstname();
    }

}
