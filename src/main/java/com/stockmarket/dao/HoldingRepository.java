package com.stockmarket.dao;

import com.stockmarket.entity.Holding;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HoldingRepository extends JpaRepository<Holding, Long> {

    @Query("select hold from Holding hold where hold.userid=?1 and hold.CompanyName=?2")
    Holding findHoldingByCompanyNameandUserid(Long userid, String companyname);

    @Query("select hold from Holding hold where hold.userid=?1")
    List<Holding> findByUserId(Long userid);
}
