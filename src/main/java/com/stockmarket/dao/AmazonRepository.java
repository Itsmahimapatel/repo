package com.stockmarket.dao;

import com.stockmarket.entity.Amazon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("http://localhost:4200")
public interface AmazonRepository extends JpaRepository<Amazon, String> {
    @Query("select distinct a from Amazon a where a.trade_date>='2021-01-01' order by a.trade_date DESC ")
    List<Amazon> findAmazonByTrade_date();
}
