package com.stockmarket.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.stockmarket.entity.Microsoft;

import java.util.List;


@CrossOrigin("http://localhost:4200")

public interface MicrosoftRepository extends JpaRepository<Microsoft, String>{
    @Query("select distinct a from Microsoft a where a.trade_date>='2021-01-01' order by a.trade_date DESC ")
    List<Microsoft> findMicrosoftByTrade_date();
}