package com.stockmarket.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.stockmarket.entity.Netflix;

import java.util.List;


@CrossOrigin("http://localhost:4200")

public interface NetflixRepository extends JpaRepository<Netflix, String>{
    @Query("select distinct a from Netflix a where a.trade_date>='2021-01-01' order by a.trade_date DESC ")
    List<Netflix> findNetflixByTrade_date();
}