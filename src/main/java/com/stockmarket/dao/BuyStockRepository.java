package com.stockmarket.dao;
import com.stockmarket.entity.BuyStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface BuyStockRepository extends JpaRepository<BuyStock,Long>{
    @Override
    <S extends BuyStock> S save(S entity);
    @Query("select b from BuyStock b where b.userid=?1")
    List<BuyStock> findBuyStockByUserid(Long userid);
}
