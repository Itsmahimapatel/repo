package com.stockmarket.dao;

import com.stockmarket.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@RepositoryRestResource(collectionResourceRel = "stock",path = "stock")
public interface StockRepository extends JpaRepository<Stock, Long> {
    @Query("select st from Stock st where st.CompanyName=?1")
    Stock findStockByCompanyName(String CompanyName);
    
    @Query("select st.CompanyName from Stock st ")
    List<String> findcompanyName();

    @Query("select st.price from Stock st ")
    List<Double> getStockPrice();



}
