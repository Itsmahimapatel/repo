package com.stockmarket.dao;

import com.stockmarket.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User,Long> {
    @Query("select u from User u where u.useremail=?1")
    User findUserByUseremail(String useremail);
}
