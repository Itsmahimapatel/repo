package com.stockmarket.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name="buystock")
public class BuyStock {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="OrderId")
    private Long OrderId;
    private Long userid;
    @Column(name="CompanyId")
    private Long CompanyId;
    @Column(name="CompanyName")
    private String CompanyName;
    @Column(name="NumberOfStocks")
    private int NumberOfStocks;
    @Column(name="StockPrice")
    private double StockPrice;
    private double TotalCost;
    private String buyorsell;
    private int statuscode;

    public BuyStock(){
    }
    public BuyStock(Long userid,Long companyId, String companyName, int numberOfStocks, double price, double totalprice,String buyorsell, int statuscode) {
        super();
        this.userid=userid;
        this.CompanyId=companyId;
        this.CompanyName=companyName;
        this.NumberOfStocks=numberOfStocks;
        this.StockPrice=price;
        this.TotalCost=totalprice;
        this.buyorsell=buyorsell;
        this.statuscode=statuscode;
    }
}
