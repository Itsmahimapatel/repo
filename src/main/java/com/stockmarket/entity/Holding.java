package com.stockmarket.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Holding {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="holdingId")
    private Long holdingId;
    private Long userid;
    @Column(name="CompanyName")
    private String CompanyName;
    @Column(name="NumberOfStocks")
    private int NumberOfStocks;
    @Column(name="buyingprice")
    private double buyingprice;
    @Column(name="total")
    private double total;

    public Holding(){}

    public Holding(Long userid,String cname, int no, double p, double tot) {
        this.userid=userid;
        this.CompanyName=cname;
        this.NumberOfStocks=no;
        this.buyingprice=p;
        this.total=tot;
    }
}
