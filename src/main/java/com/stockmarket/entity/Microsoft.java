package com.stockmarket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="microsoft")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Microsoft{
   
    @Column(name="CompanyName")
    private String CompanyName;
    
    @ManyToOne
    @JoinColumn(name="CompanyId",nullable=false)
    private Stock stock;

    @Id
    @Column(name="trade_date")
    private Date trade_date;
    
    @Column(name="open_value")
    private double open_value;

    @Column(name="volume")
    private double volume;

    @Column(name="high_value")
    private double high_value;
    
    @Column(name="low_value")
    private double low_value;
    
    @Column(name="close_value")
    private double close_value;

    @Column(name="adjustedclose")
    private double adjustedclose;

}
