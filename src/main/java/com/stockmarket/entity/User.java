package com.stockmarket.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long userId;
    private String useremail;
    private String firstname;
    private String lastname;
    private String password;
    public User(String email, String pass, String fname, String lname){
        this.useremail=email;
        this.password=pass;
        this.firstname=fname;
        this.lastname=lname;
    }
}
