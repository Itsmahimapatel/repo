package com.stockmarket.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="stock")
@Data
public class Stock {
    @Id
    @Column(name="companyId")
    private Long CompanyId;
    @Column(name="Companyname")
    private String CompanyName;
    @Column(name="price")
    private double price;

}
