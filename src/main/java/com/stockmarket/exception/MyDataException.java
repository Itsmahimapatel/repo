package com.stockmarket.exception;

public class MyDataException extends RuntimeException{
    public MyDataException(String message){
        super(message);
    }
}