package com.stockmarket.controller;

import java.util.List;

import com.stockmarket.entity.Amazon;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmarket.dao.AmazonRepository;

@RestController
public class AmazonController {

    private final AmazonRepository amazonRepository;

    public AmazonController(AmazonRepository amazonRepository) {
        this.amazonRepository = amazonRepository;
    }

    @GetMapping(value = "/stocks/amazon")
    public List<Amazon> getAllStockDetails() {
        return amazonRepository.findAmazonByTrade_date();
    }
}
