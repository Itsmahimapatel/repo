package com.stockmarket.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmarket.dao.MicrosoftRepository;
import com.stockmarket.entity.Microsoft;

@RestController
public class MicrosoftController {

    private final MicrosoftRepository microsoftRepository;

    public MicrosoftController(MicrosoftRepository microsoftRepository) {
        this.microsoftRepository = microsoftRepository;
    }

    @GetMapping(value = "/stocks/microsoft")
    public List<Microsoft> getAllStockDetails() {
        return microsoftRepository.findMicrosoftByTrade_date();
    }
}
