package com.stockmarket.controller;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmarket.dao.AppleRepository;
import com.stockmarket.entity.Apple;

@RestController
public class AppleController {

    private final AppleRepository appleRepository;

    public AppleController(AppleRepository appleRepository) {
        this.appleRepository = appleRepository;
    }

    @GetMapping(value = "/stocks/apple")
    public List<Apple> getAllStockDetails() {
        return appleRepository.findAppleByTrade_date();
    }
}
