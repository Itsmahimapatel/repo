package com.stockmarket.controller;

import com.stockmarket.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin
public class UserController {
    private final UserService userService;

    @GetMapping("/createuser")
    public int createUser(@RequestParam String useremail, @RequestParam String password, @RequestParam(required = false)String firstname, @RequestParam(required = false)String lastname){
        return userService.create(useremail,password,firstname,lastname);
    }

    @GetMapping("/login")
    public String login(@RequestParam String useremail, @RequestParam String password){
        String firstname;
        int flag=userService.login(useremail,password);
        if(flag==1) {
            firstname = userService.display();
        }
        else{
            firstname="";
        }
        return firstname;
        }
}
