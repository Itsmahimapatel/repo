package com.stockmarket.controller;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmarket.dao.FacebookRepository;
import com.stockmarket.entity.Facebook;

@RestController
public class FacebookController {

    private final FacebookRepository facebookRepository;

    public FacebookController(FacebookRepository facebookRepository) {
        this.facebookRepository = facebookRepository;
    }

    @GetMapping(value = "/stocks/facebook")
    public List<Facebook> getAllStockDetails() {
        return facebookRepository.findFacebookByTrade_date();
    }
}
	
