package com.stockmarket.controller;

import com.stockmarket.dao.*;
import com.stockmarket.entity.*;
//import com.stockmarket.exception.MyDataException;
import com.stockmarket.service.UserTransport;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;


@CrossOrigin
@RestController
public class StockController {



    Stock stock;


    private static UserTransport userTransport;
    private final BuyStockRepository buyStockRepository;

    private final StockRepository stockRepository;
    private final HoldingRepository holdingRepository;


    @Autowired
    public StockController(BuyStockRepository buyStockRepository,   StockRepository stockRepository, HoldingRepository holdingRepository){
        this.buyStockRepository=buyStockRepository;

        this.stockRepository=stockRepository;
        this.holdingRepository=holdingRepository;

    }


    @GetMapping("/holding")
    public List<Holding> persist(){
        Long userid=(userTransport.getuser()).getUserId();
        List<Holding> nullhold=null;
        List<Holding> hold=holdingRepository.findByUserId(userid);
        if(hold!=null)
            return hold;
        else
            return nullhold;
    }

    //buy?n=10&companyname=AMZN&choice=MARKET&p=3317.11
    @GetMapping("/buy")
    public BuyStock persist(@RequestParam(required = false) int n, @RequestParam(required = false) String companyname, @RequestParam(required=false) String choice, @RequestParam(required = false) double p){



        Stock st = stockRepository.findStockByCompanyName(companyname);
        Long userid=(userTransport.getuser()).getUserId();
        System.out.print(st);
        double price;
        String cn = st.getCompanyName();
        switch (choice) {
            case "LIMIT":
                price = p;
                break;
            default:
                price = st.getPrice();
        }
        double tp = price * n;



        BuyStock newstock = new BuyStock(userid,st.getCompanyId(), cn, n, price, tp,"buy",0);
        BuyStock savedObject = buyStockRepository.save(newstock);

        allow(savedObject.getOrderId(), price);
        return savedObject;


    }
    @GetMapping("/orders")
    public List<BuyStock> orders(){
        Long userid=(userTransport.getuser()).getUserId();
        return buyStockRepository.findBuyStockByUserid(userid);
    }
    public void allow(Long orderId, double price){
        BuyStock buyst=buyStockRepository.getById(orderId);
        Stock nst=stockRepository.getById(buyst.getCompanyId());
        Long userid=(userTransport.getuser()).getUserId();
        String companyname=buyst.getCompanyName();
        Holding hld=holdingRepository.findHoldingByCompanyNameandUserid(userid,companyname);

        if(buyst.getStockPrice()==nst.getPrice()){
            String cname=buyst.getCompanyName();
            int no=buyst.getNumberOfStocks();
            double p=buyst.getStockPrice();
            double tot=buyst.getTotalCost();
            if(hld==null) {
                Holding holdst = new Holding(userid,cname, no, p, tot);
                holdingRepository.save(holdst);
            }
            else{
                int num=hld.getNumberOfStocks()+buyst.getNumberOfStocks();
                double currentholdingprice=hld.getBuyingprice();
                double totalbuyingprice=((currentholdingprice*hld.getNumberOfStocks())+ buyst.getTotalCost());
                hld.setNumberOfStocks(num);
                hld.setTotal(totalbuyingprice);
                hld.setBuyingprice(totalbuyingprice/num);
                System.out.print("working");
                holdingRepository.save(hld);

            }
            buyst.setStatuscode(1);
            buyStockRepository.save(buyst);
        }
        else
        {
            buyst.setStatuscode(0);
            buyStockRepository.save(buyst);
        }
    }

    @GetMapping("/pr")
    public String index(@RequestParam(required = false) String name)
    {
        return name;
    }

    @GetMapping("/sell")
    public List<Holding> persist1(@RequestParam int numofstocks, @RequestParam String companyname, @RequestParam String choice, @RequestParam(required = false) double stoploss)
    {
        Long userid=(userTransport.getuser()).getUserId();
        Holding hold=holdingRepository.findHoldingByCompanyNameandUserid(userid,companyname);
        Stock st=stockRepository.findStockByCompanyName(companyname);
        double currentprice=st.getPrice();
        double tot=hold.getTotal();
        double buyprice=tot/hold.getNumberOfStocks();
        double sellprice=0.0;
        int currentnu=hold.getNumberOfStocks();

        switch (choice) {
            case "MARKET":
                if (numofstocks <= currentnu) {
                    sellprice = currentprice;
                    hold.setTotal(tot - sellprice * numofstocks);
                    hold.setNumberOfStocks(currentnu - numofstocks);
                    holdingRepository.save(hold);
                }
                break;
            case "LIMIT":
                if (numofstocks <= currentnu) {
                    sellprice = currentprice * (100.0 - stoploss) / 100.0;
                    hold.setTotal(tot - sellprice * numofstocks);
                    hold.setNumberOfStocks(currentnu - numofstocks);
                    holdingRepository.save(hold);
                }
                break;
        }
        BuyStock sellstock=new BuyStock(userid,st.getCompanyId(),st.getCompanyName(),numofstocks,sellprice,numofstocks*sellprice,"sell",0);
        buyStockRepository.save(sellstock);
        if(st.getPrice()==sellstock.getStockPrice())
        {
            sellstock.setStatuscode(1);
            buyStockRepository.save(sellstock);
        }
        return holdingRepository.findAll();
    }


  /*  @ExceptionHandler(MyDataException.class)
    public String handleerror(MyDataException e){
        return "redirect:/showError.html";
    }

   */

}


