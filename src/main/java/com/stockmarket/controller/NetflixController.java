package com.stockmarket.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmarket.dao.NetflixRepository;
import com.stockmarket.entity.Netflix;

@RestController
public class NetflixController {

    private final NetflixRepository netflixRepository;

    public NetflixController(NetflixRepository netflixRepository) {
        this.netflixRepository = netflixRepository;
    }

    @GetMapping(value = "/stocks/netflix")
    public List<Netflix> getAllStockDetails() {
        return netflixRepository.findNetflixByTrade_date();
    }
}